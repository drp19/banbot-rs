use serenity::{
    model::prelude::*,
    prelude::*,
};
use tracing::{warn, info, debug, error, instrument};
use std::time::{SystemTime, UNIX_EPOCH};
use rand::Rng;
use chrono::{Local, NaiveDateTime};

use crate::data::models::{BanData, BanSupplement, BanType, GuildFlags, GuildData};

#[instrument(skip(handler, ctx, add_reaction), fields(channel = %add_reaction.channel_id, msg = %add_reaction.message_id, user = %add_reaction.user_id.unwrap_or(UserId(0)), emoji = %add_reaction.emoji))]
pub async fn process(handler: &super::Handler, ctx: Context, add_reaction: Reaction) {

    // get guild
    let guild_data;
    let guild_lock;
    {
        match add_reaction.guild_id {
            Some(g) => {
                match handle_db_result!(handler.db.get_guild(g), "Getting guild data in message process") {
                    (_, None) => {return;}, // no lock present, failed on insert
                    (Some(g), Some(l)) => {
                        guild_data = g;
                        guild_lock = l;
                    },
                    (None, _) => { error!("Guild removed from DB!"); return;},
                }
            }
            None => {debug!("Recieved DM, ignoring."); return;}
        }
    }
    let _lock = guild_lock.lock().await;


    // validate that ban exists, correct emoji, get ban
    let mut ban;
    {
        let is_same = if let ReactionType::Custom { animated: _, id: i1, name: _ } = &add_reaction.emoji {
            if let ReactionType::Custom { animated: _, id: i2, name: _ } = &guild_data.get_emoji() {
                i1 == i2
            } else {
                false
            }
        } else if let ReactionType::Unicode(s1) = &add_reaction.emoji {
            if let ReactionType::Unicode(s2) = &guild_data.get_emoji() {
                s1 == s2
            } else {
                false
            }
        } else {
            false
        };
        if !is_same {
            debug!("Not vote reaction.");
            return;
        }

        // check whether the message has an active ban associated
        let mut existing_bans = handle_db_result!(handler.db.get_bans(Some(guild_data.get_guild_id()), None, None, None, None, Some(add_reaction.message_id), None), "Get existing bans");
        existing_bans = existing_bans.into_iter().filter(|b| b.get_ban_type().do_process_vote()).collect();
        ban = match existing_bans.pop() {
            Some(b) => b,
            None => {
                debug!("No activeban on message.");
                return;
            }
        };
    }

    // get vote message
    let vote_message;
    {
        vote_message = match add_reaction.message(&ctx.http).await {
            Ok(m) => m,
            Err(e) => {
                warn!(error = %e, "Could not retrieve message object!");
                return;
            }
        };
    }

    // validate ban ready
    {
        let reaction_count = match vote_message.reactions.iter().find(|r| r.reaction_type == add_reaction.emoji).map(|r| r.count) {
            Some(n) => n,
            None => {
                warn!("Could not get reaction amount!");
                return;
            }
        };
        handle_db_result!(ban.set_votes(&handler.db, reaction_count), "Update vote count");
        if ban.get_ban_type() != BanType::PENDING || reaction_count < guild_data.get_votes_needed() as u64 + 1 {
            info!("Not ban ready.");
            return;
        }
    }

    self::ban(handler, ctx, guild_data, Some(add_reaction.emoji), vote_message, &mut ban).await;
}

pub async fn ban(handler: &super::Handler, ctx: Context, guild_data: GuildData, reaction: Option<ReactionType>, message: Message, ban : &mut BanData) {
    // get guild
    let guild = match guild_data.get_guild_id().to_guild_cached(&ctx.cache).await {
        Some(g) =>g, 
        None => {
            warn!("Guild is not in cache!");
            //database_handler::change_ban_state( handler.db_tx.clone(), ban.db_id.unwrap(), BanType::FAILED).await;
            return;
        }
    };
    let own_id = ctx.cache.current_user_id().await;

    //roulette!
    if guild_data.get_flags().contains(GuildFlags::ROULETTE) {
        match ban.get_votes() {
            Some(v) => {
                let rand = rand::thread_rng().gen_range(0..(ban.get_votes().unwrap_or(0)*2));
                if rand < v && reaction.is_some() {
                    match message.reaction_users(&ctx.http, reaction.unwrap(), None, None).await {
                        Ok(v) => {
                            let non_bot: Vec<&User> = v.iter().filter(|u| u.id != own_id).collect();
                            match non_bot.get(rand as usize) {
                                Some(v) => {
                                    let new_supp = match ban.get_supplementary_data() {
                                        Some(mut s) => {
                                            s.roulette_from = Some(ban.get_target_id());
                                            s
                                        },
                                        None => BanSupplement {
                                            roulette_from: Some(v.id),
                                            tombstone_msg_source: None,
                                            tombstone_msg_sent: None,
                                        },
                                    };
                                    handle_db_result!(ban.set_supplementary_data(&handler.db, Some(new_supp)), "Roulette new supplement");
                                    handle_db_result!(ban.set_target(&handler.db, v.id), "Roulette new target");
                                    handle_db_result!(ban.set_reason(&handler.db, match ban.get_reason() { 
                                        Some(r) => Some(format!("*Roulette*: {}", r)),
                                        None => Some("*Roulette*".to_string())
                                    }), "Roulette new reason");
                                    info!("Rouletting {}", v.id);

                                },
                                None => {warn!("Roulette vec isn't big enough! Continuing.");}
                            };
                        }
                        Err(e) => {
                            warn!(error = %e, "Could not get reaction users for roulette. Continuing.");
                        }
                    }
                }
            }
            None => {
                info!("Calling roulette on a ban that has no votes? Might be @everyone ban.");
            }
        };

    }

    // get member object
    let target_member;
    {
        target_member = match guild.member(&ctx, ban.get_target_id()).await {
            Ok(m) => m,
            Err(e) => {
                warn!(error = %e, "Could not get member, they might not be in this guild anymore. Can't continue with this, need to try again.");
                //database_handler::change_ban_state( handler.db_tx.clone(), ban.db_id.unwrap(), BanType::FAILED).await;
                return;
            } 
        };
    }

    //check roles
    {
        let own_member = match ctx.cache.member(guild_data.get_guild_id(), own_id).await {
            Some(m) => Some(m),
            None=> {
                warn!("Could not get own member from cache? Going to continue anyway.");
                //database_handler::change_ban_state( handler.db_tx.clone(), ban.db_id.unwrap(), BanType::FAILED).await;
                //return;
                None
            } 
        };
        if own_member.is_some() {
            // compare highest roles
            let valid = match target_member.highest_role_info(&ctx.cache).await {
                Some((_, idx1)) => {
                    own_member.unwrap().highest_role_info(&ctx.cache).await.map(|(_,idx2)| idx2 > idx1)
                }
                None => None
            };
            if valid.is_none() {
                warn!("Could not role info from cache? Will continue without validating.");
                //database_handler::change_ban_state( handler.db_tx.clone(), ban.db_id.unwrap(), BanType::FAILED).await;
            } else if !valid.unwrap() {
                warn!("Target user has a higher permission level than the bot!");
                //handle_db_result!(ban.set_ban_type(&handler.db, BanType::INVALID), "Invalid permission end");
                super::reply_with_fallback(&ctx.http, &message, format!("Could not ban {} as the bot does not have enough permissions.\n Move the bot role above the target and try again.", target_member.display_name())).await;
                return;
            }
        } else {
            warn!("Could not validate relative permissions because of the above error. Hopefully this works!")
        }
        
        // validate member permission
        match guild.member_permissions(&ctx, own_id).await {
            Ok(p) => if !p.ban_members() {
                warn!("Bot does not have permission to ban users!");
                //database_handler::change_ban_state( handler.db.clone(), ban.db_id.unwrap(), BanType::INVALID).await;
                super::reply_with_fallback(&ctx.http, &message, format!("Could not ban {} as the bot does not have enough permissions.\nGive the bot ban permissions and try again.", target_member.display_name())).await;
                return;
            }
            Err(e) => {
                warn!(error = %e, "Could not get bot's permissions. Will assume it has it?");
                //database_handler::change_ban_state( handler.db_tx.clone(), ban.db_id.unwrap(), BanType::INVALID).await;
                return;
            }
        };
    }



    // all errors above except invalid perms are not final, i.e. another reaction event can re-trigger this
    
    info!("Attempting ban.");
    handler.kill_threads(ban.get_id());


    // send DM
    let mut can_send_dm = true;
    {
        let dm_msg = match target_member.user.create_dm_channel(&ctx.http).await {
            Ok(c) => {
                match ban.get_channel_id().create_invite(&ctx.http, |f| f.max_uses(1).unique(true)).await {
                    Ok(inv) => {
                        let pm_content = if ban.get_reason().is_some() {
                            format!("You were banned from {} for {}.\nRejoin with https://discord.gg/{}", guild.name, ban.get_reason().unwrap(), inv.code)
                        } else {
                            format!("You were banned from {}.\nRejoin with https://discord.gg/{}", guild.name, inv.code)
                        };
                        c.send_message(&ctx.http, |m| m.content(pm_content)).await
                    }
                    Err(e) => Err(e)
                }
            }
            Err(e) => Err(e)
        };
        if let Err(e) = dm_msg {
            can_send_dm = false;
            warn!(error = %e, "Could not send PM!");
            super::reply_with_fallback(&ctx.http, &message, format!("Could not private message {}. Someone will need to send a new invite.", target_member.display_name())).await;
        }
    }

    // actually ban
    {
        if let Err(e) = guild.ban(&ctx.http, ban.get_target_id(), 0).await {
            warn!(error = %e, "Could not ban!");
            handle_db_result!(ban.set_ban_type(&handler.db, BanType::FAILED), "Ban failed");
            super::reply_with_fallback(&ctx.http, &message, format!("Could not ban {}. Something went wrong.", target_member.display_name())).await;
            return;
        }
        info!("Banned user.");
    }

    // edit original message
    {
        let mut content = message.content.to_owned();
        content.push_str(" (DONE)");
        if let Err(e) = message.to_owned().edit(&ctx.http, |m| m.content(content)).await {
            warn!(error = %e, "Could not edit original message with DONE");
        }
    }

    // send confirmation message
    let ban_msg_id;
    {
        let to_send = match ban.get_reason().to_owned() {
            Some(s) => format!("{} has been vote banned for {} minutes for **{}**", target_member.display_name(), guild_data.get_ban_timeout().as_secs_f64()/60.0, s),
            None => format!("{} has been vote banned for {} minutes", target_member.display_name(), guild_data.get_ban_timeout().as_secs_f64()/60.0)
        };
        ban_msg_id = super::reply_with_fallback(&ctx.http, &message, to_send).await.map(|m| m.id);
    }

    // create thread
    {
        if guild_data.get_flags().contains(GuildFlags::TOMBSTONE) {
            // if ban confirm was successful
            if let Some(id) = ban_msg_id {
                match ban.get_target_id().create_dm_channel(&ctx).await {
                    Ok(pm) => {
                        if let Err(e) = pm.send_message(&ctx.http, |m| m.content("If you would like to send a message from beyond the grave, you may send one here.")).await {
                            warn!(err = %e, "Could not send PM for tombstone. Probably didn't join secondary server.");
                            can_send_dm = false;
                        }
                    },
                    Err(e) => {
                        warn!(err = %e, "Could not get PM for tombstone. Probably didn't join secondary server.");
                        can_send_dm = false;
                    },
                }
                // only start tombstone thread if can send PM
                if can_send_dm {
                    let mut thread = serenity::builder::CreateThread::default();
                    let now = Local::now();
        
                    let current_date = now.format("%b %d");
                    let joined_at = target_member.joined_at.map(|t| t.format("%b %d"));
                
                    let thread_name = if joined_at.is_some() { 
                        format!("RIP {} ⚰️ {} - {}", target_member.display_name(), joined_at.unwrap(), current_date)
                    } else {
                        format!("RIP {} ⚰️ {}", target_member.display_name(), current_date)
                    };
            
                    thread.name(thread_name).kind(ChannelType::PublicThread).auto_archive_duration(60);
                    match ctx.http.create_public_thread(ban.get_channel_id().0, id.0, &serenity::utils::hashmap_to_json_map(thread.0)).await {
                        Ok(ch) => {
                            if let Err(e) = ch.send_message(&ctx.http, |m | m.content(format!("Send messages to {} in this thread.", target_member.user.name))).await {
                                warn!(error = %e, "Could not send message in thread!");
                            }
                        },
                        Err(e) => {
                            warn!(error = %e, "Could not create thread!");
                        }
                    }
                }
            }
        }
    }

    //update ban
    {
        let now = SystemTime::now().duration_since(UNIX_EPOCH).expect("Time went backwards").as_millis();

        handle_db_result!(ban.set_ban_type(&handler.db, BanType::EXECUTED), "Ban executed");
        handle_db_result!(ban.set_roles(&handler.db, target_member.roles), "Ban executed");
        handle_db_result!(ban.set_nickname(&handler.db, target_member.nick), "Ban executed");
        handle_db_result!(ban.set_ban_time(&handler.db, now as u64), "Ban executed");
        handle_db_result!(ban.set_ban_message(&handler.db, ban_msg_id), "Ban executed");

        handler.add_thread(ctx.http.to_owned(), &guild_data, ban, true).await;
    }

}