#[macro_use] extern crate diesel;
#[macro_use] extern crate diesel_migrations;
#[macro_use] extern crate lazy_static;
#[macro_use] extern crate bitflags;

use std::env;
use serenity::{client::bridge::gateway::GatewayIntents, prelude::*};
use tracing::{instrument, error};

mod events;
mod data;

const DB_PATH : &str = "data/data.db";

/// banbot-rs
/// Run with DISCORD_TOKEN env set as well as
/// RUST_LOG, which is suggested to be INFO,banbot_rs=DEBUG
#[tokio::main]
#[instrument]
async fn main() {
    tracing_subscriber::fmt::init();
    // read credentials
    let token = env::var("DISCORD_TOKEN")
        .expect("Expected a token in the environment");
    // validate token
    if let Err(e) = serenity::client::validate_token(&token) {
        panic!("FATAL: Provided token is invalid! Debug: {:?}", e);
    }    
    // create database
    let db_handler = data::DBHandler::new(DB_PATH);

    // Create a new instance of the Client, logging in as a bot. This will
    // automatically prepend your bot token with "Bot ", which is a requirement
    // by Discord for bot users.
    let mut client = Client::builder(&token)
        .event_handler(events::Handler::new(db_handler))
        .intents(GatewayIntents::all())
        .await
        .expect("FATAL: Err creating client");

	// start a shard
	if let Err(e) = client.start().await {
		error!(error = %e, "Error occured creating shard.");
	}
}