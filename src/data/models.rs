use std::fmt;
use std::time::Duration;
use std::str::FromStr;
use diesel::{RunQueryDsl, ExpressionMethods};
use serenity::model::channel::ReactionType;
use serenity::model::id::EmojiId;
use serenity::model::{id::{ChannelId, GuildId, MessageId, RoleId, UserId}};
use tracing::warn;

use super::{DBHandler, bans, servers};

#[derive(Queryable, AsChangeset, Identifiable, Debug)]
#[changeset_options(treat_none_as_null="true")]
#[table_name="bans"]
pub struct DBBan {
    id: i32,
    server_id: i64,
    create_time: i64, //millis
    create_message_id: Option<i64>,
    author_id: i64,
    ban_time: Option<i64>, //millis
    vote_message_id: Option<i64>,
    rejoin_time: Option<i64>, //millis
    channel_id: i64,
    target_id: i64,
    votes: Option<i32>,
    reason: Option<String>, //MAY BE MISSING
    nickname: Option<String>, //MAY BE MISSING
    roles: Option<String>,
    ban_message_id: Option<i64>, //MAY BE MISSING
    ban_type: String,
    supplementary_data: Option<String>
}

#[derive(Insertable, Debug)]
#[table_name="bans"]
pub(super) struct InsertableBan {
    pub(super) server_id: i64,
    pub(super) create_time: i64, //millis
    pub(super) author_id: i64,
    pub(super) channel_id: i64,
    pub(super) target_id: i64,
    pub(super) ban_type: String    
}


#[derive(Queryable, AsChangeset, Identifiable, Debug, Clone)]
#[changeset_options(treat_none_as_null="true")]
#[table_name="servers"]
pub struct DBGuild {
    id: i32,
    guild_id: i64,
    first_seen: i64,
    emoji: Option<String>,
    vote_prefix: Option<String>,
    stats_prefix: Option<String>,
    lang: Option<String>,
    votes_needed: Option<i32>,
    ban_timeout: Option<i32>,
    vote_expire: Option<i32>,
    flags: i32,
    settings_prefix: Option<String>
}

#[derive(Insertable, Debug)]
#[table_name="servers"]
pub struct InsertableGuild {
    pub(super) guild_id: i64,
    pub(super) first_seen: i64,
}

#[derive(Debug)]
pub struct BanData {
    db: DBBan,
    roles: Option<Vec<RoleId>>,
    kind: BanType,
    supp: Option<BanSupplement>
}

#[derive(Debug, Clone, serde::Deserialize, serde::Serialize)]
pub struct BanSupplement {
    pub roulette_from: Option<UserId>,
    pub tombstone_msg_source: Option<MessageId>,
    pub tombstone_msg_sent: Option<MessageId>
}

#[derive(Debug, Clone)]
pub struct GuildData {
    db: DBGuild,
    emoji: ReactionType,
    flags: GuildFlags
}

bitflags! {
    pub struct GuildFlags: u16 {
        const ROULETTE = 0b00000001;
        const EVERYONE_BAN = 0b00000010;
        const TOMBSTONE = 0b00000100;
    }
}

impl BanData {
    pub fn gen_supplemental_fields(db: DBBan) -> Result<BanData, DBError> {
        let roles = match &db.roles {
            None => None,
            Some(str) => {
                let mut split = str.split(",");
                let mut out = Vec::new();
                while let Some(item) = split.next() {
                    out.push(item.parse::<u64>()
                        .map(|u| RoleId(u))
                        .map_err(|_| DBError::InvalidDataType(format!("Role {} is not valid!", item)))?);
                }
                Some(out)
            }
        };
        let kind = match BanType::from_string(db.ban_type.clone()) {
            Some(k) => k,
            None => {
                return Err(DBError::InvalidDataType(format!("Ban kind {} is not valid!", db.ban_type)));
            }
        };
        let supp: Option<BanSupplement> = match db.supplementary_data.clone() {
            Some(s) => {
                match serde_json::from_str(&s[..]) {
                    Ok(j) => Some(j),
                    Err(_) => {return Err(DBError::InvalidDataType(format!("Supplementary data {} is not valid!", s)));}
                }
            },
            None => None
        };

        Ok(BanData{
            db,
            roles,
            kind,
            supp
        })
    }

    pub fn get_id(&self) -> i32 {
        self.db.id
    }

    pub fn get_server_id(&self) -> GuildId {
        (self.db.server_id as u64).into()
    }

    pub fn get_create_time(&self) -> Duration {
        Duration::from_millis(self.db.create_time as u64)
    }

    pub fn get_create_message_id(&self) -> Option<MessageId> {
        self.db.create_message_id.map(|i| (i as u64).into())
    }

    pub fn get_author_id(&self) -> UserId {
        (self.db.author_id as u64).into()
    }

    pub fn get_ban_time(&self) -> Option<Duration> {
        self.db.ban_time.map(|t| Duration::from_millis(t as u64))
    }

    pub fn get_vote_message_id(&self) -> Option<MessageId> {
        self.db.vote_message_id.map(|t| (t as u64).into())
    }

    pub fn get_ban_message_id(&self) -> Option<MessageId> {
        self.db.ban_message_id.map(|t| (t as u64).into())
    }

    pub fn get_target_id(&self) -> UserId {
        (self.db.target_id as u64).into()
    }

    pub fn get_channel_id(&self) -> ChannelId {
        (self.db.channel_id as u64).into()
    }

    pub fn get_ban_type(&self) -> BanType {
        self.kind
    }

    pub fn get_reason(&self) -> Option<String> {
        self.db.reason.clone()
    }

    pub fn get_votes(&self) -> Option<u64> {
        self.db.votes.map(|v| v as u64).clone()
    }

    pub fn get_roles(&self) -> Option<Vec<RoleId>> {
        self.roles.clone()
    }

    pub fn get_nickname(&self) -> Option<String> {
        self.db.nickname.clone()
    }

    pub fn get_supplementary_data(&self) -> Option<BanSupplement> {
        self.supp.clone()
    }

    pub fn set_server_id(&mut self, conn: &DBHandler, id: GuildId) -> Result<(), DBError> {
        let native = id.0 as i64;

        diesel::update(&self.db)
        .set(super::bans::server_id.eq(native))
        .execute(&*conn.conn.lock().unwrap()).map(|_| ()).map_err(|e| DBError::from(e))?;

        self.db.server_id = native;
        Ok(())
    }

    pub fn set_ban_type(&mut self, conn: &DBHandler, kind: BanType) -> Result<(), DBError> {
        let native = kind.to_string();

        diesel::update(&self.db)
        .set(super::bans::ban_type.eq(&native))
        .execute(&*conn.conn.lock().unwrap()).map(|_| ()).map_err(|e| DBError::from(e))?;

        self.db.ban_type = native;
        self.kind = kind;
        Ok(())
    }

    pub fn set_reason(&mut self, conn: &DBHandler, reason: Option<String>) -> Result<(), DBError> {
        let native = reason;

        diesel::update(&self.db)
        .set(super::bans::reason.eq(&native))
        .execute(&*conn.conn.lock().unwrap()).map(|_| ()).map_err(|e| DBError::from(e))?;

        self.db.reason = native;
        Ok(())
    }

    pub fn set_vote_message(&mut self, conn: &DBHandler, msg: MessageId) -> Result<(), DBError> {
        let native = Some(msg.0 as i64);

        diesel::update(&self.db)
        .set(super::bans::vote_message_id.eq(&native))
        .execute(&*conn.conn.lock().unwrap()).map(|_| ()).map_err(|e| DBError::from(e))?;

        self.db.vote_message_id = native;
        Ok(())
    }

    pub fn set_votes(&mut self, conn: &DBHandler, votes: u64) -> Result<(), DBError> {
        let native = Some(votes as i32);

        diesel::update(&self.db)
        .set(super::bans::votes.eq(&native))
        .execute(&*conn.conn.lock().unwrap()).map(|_| ()).map_err(|e| DBError::from(e))?;

        self.db.votes = native;
        Ok(())
    }

    pub fn set_target(&mut self, conn: &DBHandler, target: UserId) -> Result<(), DBError> {
        let native = target.0 as i64;

        diesel::update(&self.db)
        .set(super::bans::target_id.eq(&native))
        .execute(&*conn.conn.lock().unwrap()).map(|_| ()).map_err(|e| DBError::from(e))?;

        self.db.target_id = native;
        Ok(())
    }

    
    pub fn set_nickname(&mut self, conn: &DBHandler, nick: Option<String>) -> Result<(), DBError> {
        let native = nick;

        diesel::update(&self.db)
        .set(super::bans::nickname.eq(&native))
        .execute(&*conn.conn.lock().unwrap()).map(|_| ()).map_err(|e| DBError::from(e))?;

        self.db.nickname = native;
        Ok(())
    }

    pub fn set_roles(&mut self, conn: &DBHandler, roles: Vec<RoleId>) -> Result<(), DBError> {
        let native = if roles.is_empty() {None} else {Some(roles.iter().map(|id| id.to_string()).collect::<Vec<String>>().join(","))};

        diesel::update(&self.db)
        .set(super::bans::roles.eq(&native))
        .execute(&*conn.conn.lock().unwrap()).map(|_| ()).map_err(|e| DBError::from(e))?;

        self.db.roles = native;
        self.roles = Some(roles);
        Ok(())
    }

    pub fn set_ban_time(&mut self, conn: &DBHandler, time: u64) -> Result<(), DBError> {
        let native = Some(time as i64);

        diesel::update(&self.db)
        .set(super::bans::ban_time.eq(&native))
        .execute(&*conn.conn.lock().unwrap()).map(|_| ()).map_err(|e| DBError::from(e))?;

        self.db.ban_time = native;
        Ok(())
    }

    pub fn set_rejoin_time(&mut self, conn: &DBHandler, time: u64) -> Result<(), DBError> {
        let native = Some(time as i64);

        diesel::update(&self.db)
        .set(super::bans::rejoin_time.eq(&native))
        .execute(&*conn.conn.lock().unwrap()).map(|_| ()).map_err(|e| DBError::from(e))?;

        self.db.rejoin_time = native;
        Ok(())
    }

    pub fn set_ban_message(&mut self, conn: &DBHandler, message: Option<MessageId>) -> Result<(), DBError> {
        let native = message.map(|m| m.0 as i64);

        diesel::update(&self.db)
        .set(super::bans::ban_message_id.eq(&native))
        .execute(&*conn.conn.lock().unwrap()).map(|_| ()).map_err(|e| DBError::from(e))?;

        self.db.ban_message_id = native;
        Ok(())
    }

    pub fn set_supplementary_data(&mut self, conn: &DBHandler, supp: Option<BanSupplement>) -> Result<(), DBError> {
        let native = match supp.clone() {
            Some(s) => Some(serde_json::to_string(&s).map_err(|e| DBError::InvalidState("Could not serialize supplement.".to_string()))?),
            None => None
        };
        diesel::update(&self.db)
        .set(super::bans::supplementary_data.eq(&native))
        .execute(&*conn.conn.lock().unwrap()).map(|_| ()).map_err(|e| DBError::from(e))?;

        self.db.supplementary_data = native;
        self.supp = supp;
        Ok(())
    }
}

impl GuildData {
    pub fn gen_supplemental_fields(db: DBGuild) -> Result<GuildData, DBError> {
        let emoji = match db.emoji.clone() {
            Some(s) => {
                match s.get(..1) {
                    Some("c") => {
                        match u64::from_str(s.get(1..).unwrap_or("NaN")) {
                            Ok(n) => Some(ReactionType::Custom { animated: false, id: EmojiId(n), name: Some("hammer".to_string())  }),
                            Err(_) => {
                                warn!("Custom emoji string is not number! Using default.");
                                None
                            }
                        }
                    },
                    Some("a") => {
                        match u64::from_str(s.get(1..).unwrap_or("NaN")) {
                            Ok(n) => Some(ReactionType::Custom { animated: true, id: EmojiId(n), name: Some("hammer".to_string())  }),
                            Err(_) => {
                                warn!("Custom emoji string is not number! Using default.");
                                None
                            }
                        }
                    }
                    Some("u") => Some(ReactionType::Unicode(s.get(1..).unwrap_or("").to_string())),
                    _ => {	
                        warn!("Emoji string must start with 'c' or 'u'. Using default");
                        None
                    }
                }
            },
            None => None
        };  
        let flags = GuildFlags::from_bits_truncate(db.flags as u16);   
        
        Ok(GuildData{
            db,
            emoji: emoji.unwrap_or(GuildData::get_default_emoji()),
            flags
        })
    }

    pub fn get_ban_timeout(&self) -> Duration {
        Duration::from_secs(self.db.ban_timeout
            .map(|i| if i == -1 { 300 } else { i } )
            .unwrap_or(300) as u64)
    }

    pub fn get_guild_id(&self) -> GuildId {
        GuildId(self.db.guild_id as u64)
    }

    pub fn get_vote_expire(&self) -> Duration {
        Duration::from_secs(self.db.vote_expire
            .map(|i| if i == -1 { 900 } else { i } )
            .unwrap_or(900) as u64)
    }

    pub fn get_vote_prefix(&self) -> String {
        self.db.vote_prefix.clone()
            .unwrap_or("?voteban".to_string())
    }

    pub fn get_stats_prefix(&self) -> String {
        self.db.stats_prefix.clone()
            .unwrap_or("/votestats".to_string())
    }

    pub fn get_settings_prefix(&self) -> String {
        self.db.settings_prefix.clone()
            .unwrap_or("/banbot".to_string())
    }

    pub fn get_votes_needed(&self) -> i32 {
        self.db.votes_needed.clone()
            .unwrap_or(3)
    }

    pub fn get_emoji(&self) -> ReactionType {
        self.emoji.clone()
    }

    pub fn get_default_emoji() -> ReactionType {
        ReactionType::Unicode("🔨".to_string())
    }

    pub fn get_flags(&self) -> GuildFlags {
        self.flags
    }

    pub fn set_flags(&mut self, conn: &DBHandler, flags: GuildFlags) -> Result<(), DBError> {
        let native = flags.bits as i32;

        diesel::update(&self.db)
        .set(super::servers::flags.eq(&native))
        .execute(&*conn.conn.lock().unwrap()).map(|_| ()).map_err(|e| DBError::from(e))?;

        self.db.flags = native;
        self.flags = flags;
        Ok(())
    }
}

#[derive(Debug)]
pub enum DBError {
	InvalidDataType(String),
	DBError(diesel::result::Error),
	InvalidState(String)
}

impl fmt::Display for DBError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
		match self {
    		DBError::InvalidDataType(s) => write!(f, "{}", s),
    		DBError::DBError(e) => write!(f, "{}", e),
			DBError::InvalidState(s) => write!(f, "{}", s),
		}
        
    }
}

impl From<diesel::result::Error> for DBError {
    fn from(err: diesel::result::Error) -> Self {
        DBError::DBError(err)
    }
}

#[derive(Debug, Clone, Copy, Eq, PartialEq)]
pub enum BanType {
    PROCESSING,
    PENDING,
	EXECUTED,
	ENDED,
	FINISHED,
	EXPIRED,
	INVALID,
	FAILED
}

//from https://stackoverflow.com/questions/32710187/how-do-i-get-an-enum-as-a-string
impl fmt::Display for BanType {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{:?}", self)
    }
}

impl BanType {
    pub fn do_process_vote (&self) -> bool {
        match *self {
            BanType::PENDING | BanType::EXECUTED => true,
            BanType::ENDED | BanType::FINISHED | BanType::EXPIRED | BanType::INVALID | BanType::FAILED | BanType::PROCESSING => false
        }
    }
    pub fn do_process_join (&self) -> bool {
        match *self {
            BanType::EXECUTED | BanType::ENDED => true,
            BanType::PENDING | BanType::FINISHED | BanType::EXPIRED | BanType::INVALID | BanType::FAILED | BanType::PROCESSING => false
        }
    }
    pub fn do_block_new_ban (&self) -> bool {
        match *self {
            BanType::PENDING | BanType::EXECUTED | BanType::ENDED => true,
            BanType::FINISHED | BanType::EXPIRED | BanType::INVALID | BanType::FAILED | BanType::PROCESSING => false
        }
    }

    pub fn from_string(string: String) -> Option<BanType> {
        match &string[..] {
            "PENDING" => Some(BanType::PENDING),
            "EXECUTED" => Some(BanType::EXECUTED),
            "ENDED" => Some(BanType::ENDED),
            "FINISHED" => Some(BanType::FINISHED),
            "EXPIRED" => Some(BanType::EXPIRED),
            "INVALID" => Some(BanType::INVALID),
            "FAILED" => Some(BanType::FAILED),
            "PROCESSING" => Some(BanType::PROCESSING),
            _ => None
        }
    }
}