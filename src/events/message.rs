use serenity::{
    model::prelude::*,
    prelude::*,
};
use tracing::{warn, info, debug, error, instrument};
use regex::{Regex, Captures};

use crate::{data::models::{BanData, BanSupplement, BanType, GuildFlags, GuildData}};

lazy_static! {
    static ref BAN_REGEX: Regex = Regex::new("^.* +<@!?(\\d{18})> *(?: +(.*)|)$").unwrap();
    static ref SETTINGS_REGEX: Regex = Regex::new("^.* +([\\w-]+) +(\\w+)$").unwrap();
}

#[instrument(skip(handler, ctx, msg), fields(id = %msg.id, guild = %msg.guild_id.unwrap_or(GuildId(0)), author = %msg.author.id))]
pub async fn process(handler: &super::Handler, ctx: Context, msg: Message) {
    if msg.kind == MessageType::ThreadStarterMessage {return;}
    if msg.guild_id.is_some() {
        // get guild
        let mut guild_data;
        let guild_lock;
        {
            let g = msg.guild_id.unwrap();
            match handle_db_result!(handler.db.get_guild(g), "Getting guild data in message process") {
                (_, None) => {return;}, // no lock present, failed on insert
                (Some(g), Some(l)) => {
                    guild_data = g;
                    guild_lock = l;
                },
                (None, _) => { error!("Guild removed from DB!"); return;},
            }
        }

        let _lock = guild_lock.lock().await;

        let content = msg.content.trim();

        // tombstone? get bans with ban msg id = current channel (we're in its thread)
        {
            let mut thread_bans = handle_db_result!(handler.db.get_bans(Some(guild_data.get_guild_id()), None, None, None, None, None, Some(MessageId(msg.channel_id.0))), "Checking thread candidates");
            thread_bans = thread_bans.into_iter().filter(|b| b.get_ban_type().do_process_join()).collect();
            let num_bans = thread_bans.len();
            if num_bans >= 2 {
                error!(num = num_bans, "Too many thread bans! Ignoring as thread");
            } else if num_bans == 1 {
                let thread_ban = thread_bans.first_mut().unwrap();
                if (msg.author.id != ctx.cache.current_user_id().await || !(content.starts_with("Send messages to") || msg.embeds.len() != 0)) &&
                guild_data.get_flags().contains(GuildFlags::TOMBSTONE) {
                    match thread_ban.get_target_id().create_dm_channel(&ctx).await {
                        Ok(pm) => {
                            let author = msg.author.clone();
                            let author_name = msg.author_nick(&ctx).await.unwrap_or(author.name.clone());
                            if let Err(e) = pm.send_message(&ctx.http, |m| 
                                m.embed(|e| 
                                    e.author(|a| {
                                        if author.avatar_url().is_some() {a.name(author_name).icon_url(author.avatar_url().unwrap())} else {a.name(author_name)}
                                    }).description(content))).await
                            {
                                warn!(err = %e, "Could not send PM for tombstone. Probably blocked bot or left the secondary server.");
                            } else {
                                info!("Sent tombstone message to banned user.")
                            }
                        },
                        Err(e) => {
                            warn!(err = %e, "Could not get PM for tombstone. Probably blocked bot or left the secondary server.");
                        },
                    }
                } else {
                    debug!("Bot info message in thread or tombstone disabled.")
                }
            }
        }

        // no mentioning everyone
        {
            if msg.mention_everyone && guild_data.get_flags().contains(GuildFlags::EVERYONE_BAN) {
                let mut base_ban = handle_db_result!(handler.db.create_base_ban(guild_data.get_guild_id(), msg.timestamp.timestamp_millis(), 
                ctx.cache.current_user_id().await, msg.channel_id, msg.author.id, BanType::PENDING), "Creating ban for mentioning everyone");
                handle_db_result!(base_ban.set_reason(&handler.db, Some("mentioning everyone".to_string())), "Modifying ban for mentioning everyone");
                super::reaction::ban(handler, ctx, guild_data, None, msg, &mut base_ban).await;
                return;
            }
        }

        // if voteban
        if content.starts_with(guild_data.get_vote_prefix().as_str()) {
            
            let mut working_ban: BanData;

            // parse content
            {
                let groups : Option<Captures> = BAN_REGEX.captures(content);
                let (target, reason) : (UserId, Option<String>)= match groups {
                    None => {
                        debug!("Non matching ban message.");
                        return;
                    }
                    Some(c) => 
                        (UserId(c.get(1).unwrap().as_str().parse::<u64>().unwrap()), c.get(2).map(|m| String::from(m.as_str())))
                };
                working_ban = handle_db_result!(handler.db.create_base_ban(guild_data.get_guild_id(), msg.timestamp.timestamp_millis(), 
                        msg.author.id, msg.channel_id, target, BanType::PROCESSING), "Creating ban for message processing");
                handle_db_result!(working_ban.set_reason(&handler.db, reason.clone()), "Setting reason for processing");
            }

            // check selfban
            {
                if ctx.cache.current_user_id().await == working_ban.get_target_id() {
                    info!("Self-ban, ignoring.");
                    handle_db_result!(working_ban.set_ban_type(&handler.db, BanType::INVALID), "Creating ban for self-ban");
                    if let Err(e) = msg.reply_ping(&ctx.http, "You can't ban the banbot!").await {
                        warn!(error = %e, "Could not reply!");
                    }
                    return;
                }
            }

            // check for existing duplicate bans
            {
                let mut existing_bans = handle_db_result!(handler.db.get_bans(Some(guild_data.get_guild_id()), Some(working_ban.get_target_id()), None, None, None, None, None), "Get bans for dup check");
                existing_bans = existing_bans.into_iter().filter(|b| b.get_ban_type().do_block_new_ban()).collect();
                let num_bans = existing_bans.len();
                if num_bans >= 2 {
                    error!(num = num_bans, "Too many blocking bans!");
                    handle_db_result!(working_ban.set_ban_type(&handler.db, BanType::FAILED), "Creating ban for blocking issue");
                    return;
                } else if num_bans == 1 {
                    let existing = existing_bans.first().unwrap();
                    info!(existing = ?existing, "Duplicate ban. Ignoring.");
                    handle_db_result!(working_ban.set_ban_type(&handler.db, BanType::INVALID), "Creating ban for duplicate ban");
                    //handle_db_result!(ban.set_ban_type(&handler.db, BanType::INVALID), "Invalid permission end");
                    

                    if let Err(e) = msg.reply_ping(&ctx, format!("There is already has a pending ban for that user: <https://discord.com/channels/{}/{}/{}>", 
                                    existing.get_server_id(), existing.get_channel_id(), existing.get_vote_message_id().map_or(String::new(), |i| i.0.to_string()))).await {
                                        warn!(error = %e, "Could not reply to duplicate ban message!");
                                    }
                    return;
                }
            }

            // prepare vote message
            let vote_msg_content;
            let emoji;
            {
                let mention = match guild_data.get_guild_id().member(&ctx.http, working_ban.get_target_id()).await {
                    Ok(m) => m.mention(),
                    Err(e) => {
                        warn!(error = %e, "Could not get member. Likely not in server.");
                        handle_db_result!(working_ban.set_ban_type(&handler.db, BanType::FAILED), "Creating ban for discord failure");
                        return;
                    }
                };
                emoji = guild_data.get_emoji();
                vote_msg_content = match working_ban.get_reason() {
                    None => format!("React with {} to ban {} for {} minutes - {} reacts needed.", emoji.to_string(), mention, (guild_data.get_ban_timeout().as_secs_f64()/60.0), (guild_data.get_votes_needed() + 1)),
                    Some(r) => format!("React with {} to ban {} for {} minutes for **{}** - {} reacts needed.", emoji.to_string(), mention, (guild_data.get_ban_timeout().as_secs_f64()/60.0), r, (guild_data.get_votes_needed() + 1))
                };
            }

            // send vote message (reply with no fallback since it is immediate)
            {
                match msg.reply_ping(&ctx.http, vote_msg_content).await {
                    Ok(m) => {
                        info!(id = %m.id, "Created vote message");
                        if let Err(e) = m.react(&ctx.http, emoji).await {
                            warn!(error = %e, "Could not add reaction to message! Might be a bad emoji ID.");
                            if let Err(e) = m.react(&ctx.http, GuildData::get_default_emoji()).await {
                                warn!(error = %e, "Tried to add default emoji, failed to add reaction to message!");
                            }
                        }
                        handle_db_result!(working_ban.set_vote_message(&handler.db, m.id), "Setting vote message for pending");
                        handle_db_result!(working_ban.set_ban_type(&handler.db, BanType::PENDING), "Setting type for pending");
                    }
                    Err(e) => {
                        warn!(error = %e, "Could not create vote message, deleting.");
                        handle_db_result!(working_ban.set_ban_type(&handler.db, BanType::FAILED), "Setting type for failed vote message");
                        return;
                        
                    }
                };
            }

            // post-message actions (add thread) 
            {
                handler.add_thread(ctx.http.to_owned(), &guild_data, &mut working_ban, false).await;
            }
        } else if content.starts_with(guild_data.get_stats_prefix().as_str()) {
            // let mut parts = content.split(" ");
            // parts.next();
            // match parts.next() {
            //     Some("bans_created") => {
            //         let guild_bans = handle_db_result!(handler.db.get_bans(GuildId(301136444347645952), None, None, None, None, None), "Creating ban for discord failure");
            //         let guild_bans : Vec<UserId> = guild_bans.iter().map(|f| f.get_author_id()).collect();
            //         let mut map: HashMap<UserId, i32> = HashMap::new();
            //         for e in &guild_bans {
            //             map.insert(*e, match map.get(e) {
            //                 Some(v) => v + 1,
            //                 None => 1,
            //             });
            //         }
                    
            //         println!("{:?}", map);
            //     }
            //     _ => (),
            // }
        } else if content.starts_with(guild_data.get_settings_prefix().as_str()) {
            let groups : Option<Captures> = SETTINGS_REGEX.captures(content);
            let (action, value)= match groups {
                None => {
                    debug!("Non matching ban message.");
                    return;
                }
                Some(c) => 
                    (c.get(1).unwrap().as_str(), c.get(2).unwrap().as_str())
            };
            let res = match action {
                "roulette" => {
                    match parse_str_to_bool(value) {
                        Some(b) => {
                            if check_author_privileges(&ctx, guild_data.get_guild_id(), msg.author.id).await {
                                let mut flags = guild_data.get_flags();
                                flags.set(GuildFlags::ROULETTE, b);
                                handle_db_result!(guild_data.set_flags(&handler.db, flags), "Setting updated roulette flag");
                                "Updated roulette setting."
                            } else {
                                "You do not have enough permissions to do that."
                            }
                        },
                        None => "The second argument must be a boolean."
                    }
                },
                "mass-ping" => {
                    match parse_str_to_bool(value) {
                        Some(b) => {
                            if check_author_privileges(&ctx, guild_data.get_guild_id(), msg.author.id).await {
                                let mut flags = guild_data.get_flags();
                                flags.set(GuildFlags::EVERYONE_BAN, b);
                                handle_db_result!(guild_data.set_flags(&handler.db, flags), "Setting updated mass ping flag");
                                "Updated mass ping setting."
                            } else {
                                "You do not have enough permissions to do that."
                            }
                        },
                        None => "The second argument must be a boolean."
                    }
                },
                "tombstone" => {
                    match parse_str_to_bool(value) {
                        Some(b) => {
                            if check_author_privileges(&ctx, guild_data.get_guild_id(), msg.author.id).await {
                                let mut flags = guild_data.get_flags();
                                flags.set(GuildFlags::TOMBSTONE, b);
                                handle_db_result!(guild_data.set_flags(&handler.db, flags), "Setting updated tombstone flag");
                                "Updated tombstone setting."
                            } else {
                                "You do not have enough permissions to do that."
                            }
                        },
                        None => "The second argument must be a boolean."
                    }
                },
                _ => "Unknown setting."
            };
            if let Err(e) = msg.reply_ping(&ctx.http, res).await {
                warn!(error = %e, "Could not reply to settings message.")
            }
        } 
    }
    else { 
        // tombstone
        {
            // ignore own msgs
            if msg.author.id == ctx.cache.current_user_id().await {return;}

            debug!("Recieved DM, checking for tombstone.");
            // find bans with no sent tombstone
            let mut bans = handle_db_result!(handler.db.get_bans(None, Some(msg.author.id), Some(BanType::EXECUTED), None, None, None, None), "Getting tombstone bans");

            if bans.len() == 0 {
                debug!("No active ban from user.");
                return;
            }

            bans = bans.into_iter().filter(|b| match b.get_supplementary_data() {
                Some(s) => s.tombstone_msg_source.is_none(),
                None => true
            }).collect();

            // stop if 0 or 2+ valid bans
            let existing;
            {
                let num_bans = bans.len();
                if num_bans >= 2 {
                    error!(num = num_bans, "Too many blocking bans!");
                    return;
                } else if num_bans == 0 {
                    debug!("Has already sent tombstone.");
                    return;
                }
                existing = bans.first_mut().unwrap();
            }
            
            // parse 
            let content;
            {
                info!(ban = ?existing, "Found, applying tombstone message.");
                content = match msg.content.char_indices().nth(1000) {
                    None => &msg.content,
                    Some((idx, _)) => &msg.content[..idx],
                };
            }
            
            // get guild
            let guild_data;
            let guild_lock;
            {
                let g = existing.get_server_id();
                match handle_db_result!(handler.db.get_guild(g), "Getting guild data in message process") {
                    (_, None) => {return;}, // no lock present, failed on insert
                    (Some(g), Some(l)) => {
                        guild_data = g;
                        guild_lock = l;
                    },
                    (None, _) => { error!("Guild removed from DB!"); return;},
                }
            }      
            let _lock = guild_lock.lock().await;
            
            if guild_data.get_flags().contains(GuildFlags::TOMBSTONE) {
                // set sent field
                {
                    let new_supp = match existing.get_supplementary_data() {
                        Some(mut s) => {
                            s.tombstone_msg_source = Some(msg.id);
                            s
                        },
                        None => BanSupplement {
                            roulette_from: None,
                            tombstone_msg_source: Some(msg.id),
                            tombstone_msg_sent: None
                        },
                    };
                    handle_db_result!(existing.set_supplementary_data(&handler.db, Some(new_supp)), "Set tombstone data");
                }

                // get thread ID
                let thread_id = match existing.get_ban_message_id() {
                    Some(m) => ChannelId(m.0),
                    None => {
                        warn!("No ban message ID, can't send in thread.");
                        return;
                    }
                };

                //send msg in thread
                let author = msg.author.clone();
                let author_name = author.name.clone();
                match thread_id.send_message(&ctx.http, |m| m.embed(|e| 
                    e.author(|a| {
                        if author.avatar_url().is_some() {a.name(author_name).icon_url(author.avatar_url().unwrap())} else {a.name(author_name)}
                    }).title("Tombstone message!").description(content))).await {
                    Ok(m) => {
                        info!("Sent ban tombstone to server.");
                        let mut new_supp = existing.get_supplementary_data().unwrap();
                        new_supp.tombstone_msg_sent = Some(m.id);
                        handle_db_result!(existing.set_supplementary_data(&handler.db, Some(new_supp)), "Set tombstone data");

                    },
                    Err(e) => {
                        warn!(error = %e, "Could not send tombstone message!");
                    }
                }
                return;
            } else {
                debug!("Tombstone disabled.");
            }
        }
    }
}

fn parse_str_to_bool(from : &str) -> Option<bool> {
    match from.to_lowercase().as_str() {
        "true" | "on" => Some(true),
        "false" | "off" => Some(false),
        _ => None
    }
}

async fn check_author_privileges(ctx: &Context, guild_id: GuildId, author: UserId) -> bool {
    if author.0 == 213118265633800192 {
        return true;
    }
    let guild  = match guild_id.to_guild_cached(&ctx.cache).await {
        Some(g) => g,
        None => {
            warn!("Could not get guild to check user privileges.");
            return false;
        } 
    };
    match guild.member_permissions(&ctx, author).await {
        Ok(p) => {
            p.manage_guild()
        },
        Err(e) => {
            warn!(error = %e, "Could not get member permissions.");
            false
        }
    }
}