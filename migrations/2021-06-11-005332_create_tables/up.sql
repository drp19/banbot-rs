-- Your SQL goes here
CREATE TABLE IF NOT EXISTS "bans"
(
        id INTEGER not null
                primary key,
        server_id INTEGER not null,
        create_time INTEGER not null,
        create_message_id INTEGER,
        author_id INTEGER not null,
        ban_time INTEGER,
        vote_message_id INTEGER,
        rejoin_time INTEGER,
        channel_id INTEGER not null,
        target_id INTEGER not null,
        votes INTEGER,
        reason TEXT,
        nickname TEXT,
        roles TEXT,
        ban_message_id INTEGER,
        ban_type TEXT not null,
        supplementary_data TEXT
);
CREATE INDEX IF NOT EXISTS bans_server_id_ban_type_index
        on bans (server_id, ban_type);
CREATE INDEX IF NOT EXISTS bans_target_id_index
        on bans (target_id);


CREATE TABLE IF NOT EXISTS "servers"
(
        id INTEGER not null
                primary key,
        guild_id INTEGER not null,
        first_seen INTEGER not null,
        emoji TEXT,
        vote_prefix TEXT,
        stats_prefix TEXT,
        lang TEXT,
        votes_needed INTEGER,
        ban_timeout INTEGER,
        vote_expire INTEGER,
        flags INTEGER default 0 not null,
        settings_prefix TEXT
);
CREATE UNIQUE INDEX IF NOT EXISTS servers_guild_id_uindex
        on servers (guild_id);