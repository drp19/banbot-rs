# Alpine Linux
FROM alpine:latest
# copy exe into image
COPY banbot-rs /bin/banbot-rs
# run application with this command line
ENTRYPOINT ["/bin/banbot-rs"]
