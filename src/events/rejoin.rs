use serenity::{
    model::prelude::*,
    prelude::*,
};
use tracing::debug;
use tracing::{warn, info, error, instrument};
use std::time::{SystemTime, UNIX_EPOCH};
use serde_json::Value;

use crate::data::models::BanType;

#[instrument(skip(handler, ctx))]
pub async fn process(handler: &super::Handler, ctx: Context, guild_id: GuildId, new_member: Member) {
    info!("Recieved member");
    
    // get guild
    let guild_data;
    let guild_lock;
    {
        match handle_db_result!(handler.db.get_guild(guild_id), "Getting guild data in message process") {
            (_, None) => {return;}, // no lock present, failed on insert
            (Some(g), Some(l)) => {
                guild_data = g;
                guild_lock = l;
            },
            (None, _) => { error!("Guild removed from DB!"); return;},
        }
    }

    let _lock = guild_lock.lock().await;

    let mut ban;
    {
        // check whether the message has an active ban associated
        let mut existing_bans = handle_db_result!(handler.db.get_bans(Some(guild_data.get_guild_id()), Some(new_member.user.id), None, None, None, None, None), "Get existing bans");
        existing_bans = existing_bans.into_iter().filter(|b| b.get_ban_type().do_process_join()).collect();
        ban = match existing_bans.pop() {
            Some(b) => b,
            None => {
                debug!("No active ban on message.");
                return;
            }
        };
    }

    handler.kill_threads(ban.get_id());

    // try to archive thread
    {
        if ban.get_ban_message_id().is_some() {
            let thread_channel = ChannelId(ban.get_ban_message_id().unwrap().0);
            match thread_channel.edit(&ctx.http, |e| {
                e.0.insert("archived", Value::Bool(true));
                e
            }).await
            {
                Ok(_) => {
                    info!("Archived thread.");
                },
                Err(e) => {
                    warn!(err = %e, "Could not archive thread. Probably doesn't exist, but could be anything.");
                }
            }
        } else {
            warn!("No ban ID?");
        }
    }

    // edit user
    {
        let roles = ban.get_roles();
        let nick = ban.get_nickname(); 
        if roles.is_some() || nick.is_some() {
            if let Err(e) = new_member.edit(&ctx.http, |e| if nick.is_some() && roles.is_some() {
                e.roles(roles.unwrap()).nickname(nick.unwrap())
            } else if roles.is_some() {
                e.roles(roles.unwrap())
            } else {
                e.nickname(nick.unwrap())
            }).await{
                warn!(error = %e, "Could not edit user!");
        
                let reply_to = match ban.get_ban_message_id() {
                    Some(id) => {
                        Some(ctx.http.get_message(ban.get_channel_id().0, id.0).await)
                    }
                    None => None
                }.map(|res| match res {
                    Ok(m) => Some(m),
                    Err(e) => {
                        warn!(error = %e, "Could not get ban message!");
                        None
                    }
                }).flatten();
                match reply_to {
                    Some(m) => {super::reply_with_fallback(&ctx.http, &m, format!("Could not add all of the permissions for {}.", new_member.display_name())).await;}
                    None => {
                        if let Err(e) = ban.get_channel_id().send_message(&ctx.http, |m| m.content(format!("Could not add all of the permissions for {}.", new_member.display_name()))).await {
                            warn!(error = %e, "Could not send warning!");
                        }
                    }
                }
            } 
            info!("Done editing user.");
        } else {
            info!("Nothing to do.")
        }
    }

    //update object
    {
        let now = SystemTime::now().duration_since(UNIX_EPOCH).expect("Time went backwards").as_millis();
        
        handle_db_result!(ban.set_ban_type(&handler.db, BanType::FINISHED), "Ban finished");
        handle_db_result!(ban.set_rejoin_time(&handler.db, now as u64), "Ban finished");
    }
}