pub mod schema;
pub mod models;

use std::{collections::HashMap, sync::{Arc, Mutex, RwLock}, time::{SystemTime, UNIX_EPOCH}};

use diesel::{prelude::*};
use schema::{bans, servers};
use models::*;
use serenity::{model::id::{ChannelId, GuildId, MessageId, UserId}};

no_arg_sql_function!(last_insert_rowid, diesel::types::Bigint);
embed_migrations!();

#[derive(Clone)]
pub struct DBHandler {
	conn: Arc<Mutex<SqliteConnection>>,
    locks: Arc<RwLock<HashMap<GuildId, Arc<tokio::sync::Mutex<()>>>>>
}

impl DBHandler {
    //PANICS
	pub fn new(db_url: &str) -> DBHandler {
        let conn = SqliteConnection::establish(&db_url)
        .expect(&format!("Error connecting to {}", db_url));
        embedded_migrations::run(&conn)
        .expect("Could not create tables for DB");
		DBHandler {
            conn: Arc::new(Mutex::new(conn)),
            locks: Arc::new(RwLock::new(HashMap::new()))
        }
	}

    pub fn create_base_ban(&self, server_id: GuildId, create_time: i64,
                        author_id: UserId, channel_id: ChannelId, target_id: UserId, ban_type: BanType) -> Result<BanData, DBError>{
        let new_ban = InsertableBan {
            server_id: server_id.0 as i64,
            create_time: create_time,
            author_id: author_id.0 as i64,
            channel_id: channel_id.0 as i64,
            target_id: target_id.0 as i64,
            ban_type: ban_type.to_string(),
        }; 
        let generated_id: i64;
        {
            let conn = &*self.conn.lock().unwrap();
            diesel::insert_into(bans::table).values(new_ban).execute(conn)?;
            generated_id = diesel::select(last_insert_rowid).first(conn)?;
        }        
        self.get_ban(generated_id as i32)
    }

    pub fn get_guild(&self, id : GuildId) -> Result<(Option<GuildData>, Option<Arc<tokio::sync::Mutex<()>>>), DBError> {
        let g = match servers::table.filter(servers::guild_id.eq(id.0 as i64)).limit(1)
        .load::<DBGuild>(&*self.conn.lock().unwrap())?.pop() {
            Some(g) => Some(GuildData::gen_supplemental_fields(g)?),
            None => None
        };
        let lock; 
        {
            let map = self.locks.read().unwrap();
            lock = map.get(&id).map(|l| l.clone());
        }
        Ok((g, lock))
    }

    pub fn process_join_guild(&self, id : GuildId) -> Result<(bool, GuildData), DBError>  {
        let res = match self.get_guild(id)?.0 {
            Some(g) => (false, g),
            None => {
                let new_guild = InsertableGuild {
                    guild_id: id.0 as i64,
                    first_seen: SystemTime::now().duration_since(UNIX_EPOCH).expect("Time went backwards").as_millis() as i64
                };
                diesel::insert_into(servers::table).values(new_guild).execute(&*self.conn.lock().unwrap())?;
                let g= match self.get_guild(id)?.0 {
                    Some(g) => g,
                    None => { return Err(DBError::InvalidState("Insertion failed??".to_string())); }
                };
                (true, g)
            }
		};
        let lock;
        {
            let map = self.locks.read().unwrap();
            lock = map.get(&id).map(|v| v.clone());
        }
        if lock.is_none() {
            let res = Arc::new(tokio::sync::Mutex::new(()));
            self.locks.write().unwrap().insert(id, res.clone());
        };
        Ok(res)
	}

    pub fn get_ban(&self, id: i32) -> Result<BanData, DBError> {
        let db_ban = bans::table.find(id as i32).first::<DBBan>(&*self.conn.lock().unwrap()).map_err(|e| DBError::DBError(e))?;
        BanData::gen_supplemental_fields(db_ban)
    }

    pub fn get_bans(&self, guild: Option<GuildId>, target: Option<UserId>, kind: Option<BanType>, min_create_time: Option<u64>, 
                    min_ban_time: Option<u64>, vote_message: Option<MessageId>, ban_message: Option<MessageId>) -> Result<Vec<BanData>, DBError> {
        let mut query_builder = bans::table.into_boxed();
        if guild.is_some() {
            query_builder = query_builder.filter(bans::server_id.eq(guild.unwrap().0 as i64));
        }
        if target.is_some() {
            query_builder = query_builder.filter(bans::target_id.eq(target.unwrap().0 as i64));
        }
        if kind.is_some() {
            query_builder = query_builder.filter(bans::ban_type.eq(kind.unwrap().to_string()));
        }
        if min_create_time.is_some() {
            query_builder = query_builder.filter(bans::create_time.lt(min_create_time.unwrap() as i64));
        }
        if min_ban_time.is_some() {
            query_builder = query_builder.filter(bans::ban_time.lt(min_ban_time.unwrap() as i64));
        }
        if vote_message.is_some() {
            query_builder = query_builder.filter(bans::vote_message_id.eq(vote_message.unwrap().0 as i64));
        }
        if ban_message.is_some() {
            query_builder = query_builder.filter(bans::ban_message_id.eq(ban_message.unwrap().0 as i64));
        }
        match query_builder.load::<DBBan>(&*self.conn.lock().unwrap()) {
            Ok(v) => {
                let mut bans = Vec::new();
                for b in v {
                    bans.push(BanData::gen_supplemental_fields(b)?);
                }
                Ok(bans)
            },
            Err(e) => Err(e.into()),
        }
    }
}


