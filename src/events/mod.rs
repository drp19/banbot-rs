use serenity::{
    async_trait,
    model::prelude::*,
    prelude::*,
    http::Http
};
use tokio::task::JoinHandle;
use tracing::{debug, error, info, instrument, warn};
use std::{sync::{Arc, Mutex}};
use std::time::{SystemTime, UNIX_EPOCH};
use std::collections::HashMap;

use crate::data::models::{BanData, GuildData, BanType};

//use super::models::*;
use super::data::DBHandler;

#[macro_export]
macro_rules! handle_db_result {
    ($res:expr, $reason:expr) => {
        {
            match $res {
                Ok(d) => d,
                Err(e) => {error!(error = %e, "DB error during: {}", $reason); return;}
            }
        }
    };
}

mod message;
mod reaction;
mod rejoin;



pub struct Handler {
    db : DBHandler,
    threads: Arc<Mutex<HashMap<i32, JoinHandle<()>>>> //indexed by db_id
}

impl Handler {
	pub fn new(db : DBHandler) -> Handler {
        Handler { db, threads : Arc::new(Mutex::new(HashMap::new())) }
    }

    pub fn kill_threads(&self, db_id : i32) -> bool {
        self.threads.lock().unwrap().remove(&db_id).map(|t| t.abort()).is_some()
    }

    pub async fn add_thread(&self, http: Arc<Http>, guild: &GuildData, ban : &mut BanData, is_ban : bool) {
        // calculate expiry time
        let expires_in;
        {
            let now = SystemTime::now().duration_since(UNIX_EPOCH).expect("Time went backwards");
            if is_ban {
                expires_in = (match ban.get_ban_time() {
                    Some(d) => d,
                    None => {
                        warn!(ban = ?ban, "Thread request has no ban time. Continuing and using create time.");
                        ban.get_create_time()
                    }
                } + guild.get_ban_timeout()).checked_sub(now)
            } else {
                expires_in = (ban.get_create_time() + guild.get_vote_expire()).checked_sub(now)
            }
        }

        match expires_in {
            Some(d) => {
                info!("Creating wait timer");
                let db_id = ban.get_id();
                let thread_cpy = self.threads.to_owned();
                let db_cpy = self.db.clone();
                let thread = tokio::spawn(async move {
                        tokio::time::sleep(d).await;
                        let mut ban = handle_db_result!(db_cpy.get_ban(db_id), format!("creating threads for {}", db_id));
                        if is_ban {
                            unban(&http, &db_cpy, &mut ban).await;
                        } else {
                            expire(&http, &db_cpy, &mut ban).await;
                        }
                        thread_cpy.lock().unwrap().remove(&db_id);
                    });
                self.threads.lock().unwrap().insert(db_id, thread);
            }
            None => {
                info!("Wait timer = 0. Immediately finishing.");
                if is_ban {
                    unban(&http, &self.db, ban).await;
                } else {
                    expire(&http, &self.db, ban).await;
                }
            }
        }
        
    }
}


#[async_trait]
impl EventHandler for Handler {
    async fn message(&self, ctx: Context, new_message: Message) {
        message::process(self, ctx, new_message).await;
    }

    async fn reaction_add(&self, ctx: Context, add_reaction: Reaction) {
        reaction::process(self, ctx, add_reaction).await;
    }

    async fn guild_member_addition(&self, ctx: Context, guild_id: GuildId, new_member: Member) {
        rejoin::process(self, ctx, guild_id, new_member).await;
    }

    #[instrument(skip(self, ctx, guild, _is_new), fields(%guild.id))]
    async fn guild_create(&self, ctx: Context, guild: Guild, _is_new: bool) {
        info!("Recieved guild_create");
        match self.db.process_join_guild(guild.id) {
            Ok((true, _)) => {info!("Added new guild.");}
            Ok((false, g)) => {
                info!("Joined existing guild.");
                let now = SystemTime::now().duration_since(UNIX_EPOCH).expect("Time went backwards");
                let create_expired_at = now.checked_sub(g.get_vote_expire()).map_or(0, |d| d.as_millis());
                let ban_expired_at = now.checked_sub(g.get_ban_timeout()).map_or(0, |d| d.as_millis());
                match self.db.get_bans(Some(g.get_guild_id()), None, Some(BanType::PENDING), None, None, None, None) {
                    Ok(bans) => {
                        debug!("Found {} pending bans.", bans.len());
                        for mut ban in bans {
                            if ban.get_create_time().as_millis() < create_expired_at {
                                expire(&ctx.http, &self.db, &mut ban).await;
                            } else {
                                self.add_thread(ctx.http.to_owned(), &g, &mut ban, false).await;
                            }
                        }
                    }
                    Err(e) => { error!(error = %e, "Error getting bans to create threads."); return;}
                }
                match self.db.get_bans(Some(g.get_guild_id()), None, Some(BanType::EXECUTED), None, None, None, None) {
                    Ok(bans) => {
                        debug!("Found {} executed bans.", bans.len());
                        for mut ban in bans {
                            let ban_time = match ban.get_ban_time() {
                                Some(t) => t,
                                None => {
                                    warn!("Executed ban does not have a ban time. Will use the create time.");
                                    ban.get_create_time()
                                }
                            };
                            if ban_time.as_millis() < ban_expired_at {
                                unban(&ctx.http, &self.db, &mut ban).await;
                            } else {
                                self.add_thread(ctx.http.to_owned(), &g, &mut ban, true).await;
                            }
                        }
                    }
                    Err(e) => { error!(error = %e, "Error getting bans to create threads."); return;}
                }
            }
            Err(e) => {error!(error=%e, "Could not join guild! Future guild events will cause warnings.")}
        }
    }

    async fn ready(&self, _ctx: Context, ready: Ready) {
        info!(user = %ready.user.name, guilds = ready.guilds.len(), presences = ready.presences.len(), pms = ready.private_channels.len(), "Connected!");
    }
 
}

#[instrument(skip(http, db))]
pub async fn unban(http: &Arc<Http>, db: &DBHandler, ban: &mut BanData) {
    info!("Unbanning");
    if let Err(e) = http.remove_ban(ban.get_server_id().0, ban.get_target_id().0).await {
        warn!(error = %e, "Unbanning failed!");
    }
    match ban.get_target_id().create_dm_channel(http).await {
        Ok(pm) => {
            if let Err(e) = pm.send_message(&http, |m| m.content("Hey! You've been unbanned.")).await {
                warn!(err = %e, "Could not send PM to notify. Probably didn't join secondary server.");
            }
        },
        Err(e) => {
            warn!(err = %e, "Could not get PM to notify. Probably didn't join secondary server.");
        },
    }
    handle_db_result!(ban.set_ban_type(&db, BanType::ENDED), "Unban");
}

#[instrument(skip(http, db))]
pub async fn expire(http: &Arc<Http>, db: &DBHandler, ban: &mut BanData) {
    info!("Ban expired");
    if ban.get_ban_type() != BanType::PENDING {
        warn!("Ban is no longer the correct type.")
    } else {
        match ban.get_vote_message_id() {
            Some(id) => {
                match http.get_message(ban.get_channel_id().0, id.0).await {
                    Ok(mut m) => {
                        let mut content = m.content.to_owned();
                        content.push_str(" (EXPIRED)");
                        if let Err(e) = m.edit(http, |e| e.content(content)).await {
                            warn!(error = %e, "Could not edit original message with EXPIRED.");
                        }
                    },
                    Err(e) => {
                        warn!(error = %e, "Could not get original message to edit.");
                    }
                };

            }
            None => {
                warn!("Ban has no vote message, could not edit.")
            }
        }
        handle_db_result!(ban.set_ban_type(&db, BanType::EXPIRED), "Expire");
    }
}

pub async fn reply_with_fallback(http: &Arc<Http>, msg: &Message, content: String) -> Option<Message> {
    match msg.reply_ping(http, &content).await {
        Ok(m) => Some(m),
        Err(e) => {
            warn!(error = %e, "Could not reply!");
            match msg.channel_id.send_message(http, |f| f.content(&content)).await {
                Ok(m) => Some(m),
                Err(e) => {
                    warn!(error = %e, "Could not send message!");
                    None
                }
            }
        }
    }
}