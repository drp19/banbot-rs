table! {
    bans (id) {
        id -> Integer,
        server_id -> BigInt,
        create_time -> BigInt,
        create_message_id -> Nullable<BigInt>,
        author_id -> BigInt,
        ban_time -> Nullable<BigInt>,
        vote_message_id -> Nullable<BigInt>,
        rejoin_time -> Nullable<BigInt>,
        channel_id -> BigInt,
        target_id -> BigInt,
        votes -> Nullable<Integer>,
        reason -> Nullable<Text>,
        nickname -> Nullable<Text>,
        roles -> Nullable<Text>,
        ban_message_id -> Nullable<BigInt>,
        ban_type -> Text,
        supplementary_data -> Nullable<Text>,
    }
}

table! {
    servers (id) {
        id -> Integer,
        guild_id -> BigInt,
        first_seen -> BigInt,
        emoji -> Nullable<Text>,
        vote_prefix -> Nullable<Text>,
        stats_prefix -> Nullable<Text>,
        lang -> Nullable<Text>,
        votes_needed -> Nullable<Integer>,
        ban_timeout -> Nullable<Integer>,
        vote_expire -> Nullable<Integer>,
        flags -> Integer,
        settings_prefix -> Nullable<Text>,
    }
}